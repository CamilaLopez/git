class Direccion:
    def __init__(self, calleNumero, provincia, codigoPostal):
        self.__calleNumero = calleNumero
        self.__provincia =  provincia
        self.__codigoPostal = codigoPostal

    def getCalleNumero(self):
        return self.__calleNumero

    def setCalleNumeo(self, nuevoCalleNumero):
        self.__calleNumero = nuevoCalleNumero
        return self.__calleNumero
    
    def getProvincia(self):
        return self.__provincia

    def setProvincia(self, nuevaProvincia):
        self.__provincia = nuevaProvincia
        return self.__provincia
    
    def getCodigoPostaal(self):
        return self.__codigoPostal 

    def setCodigoPostal(self, nuevoCodigoPostal):
        self.__codigoPostal = nuevoCodigoPostal
        return self.__codigoPostal
    
    def __str__(self):
        return  "\n" + "Su direccion es: " + str(self.getCalleNumero()) + "\n" + "Provincia: " + str(self.getProvincia()) + "\n" + "Codigo Postal: " + str(self.getCodigoPostaal())