class Contacto:
    def __init__(self, nombres, apellidos, telefonoMovil, direccion):
        self.__nombre = nombres
        self.__apellidos = apellidos
        self.__numeroMovil=telefonoMovil
        self.direccionContacto = direccion
    
    def getNombre(self):
        return self.__nombre
    
    def setNombre(self, nuevoNombre):
        self.__nombre = nuevoNombre
        return self.__nombre
    
    def getApellidos(self):
        return self.__apellidos
    
    def setApellidos(self, nuevoApellido):
        self.__apellidos = nuevoApellido
        return self.__apellidos
    
    def getNumeroMovil (self):
        return self.__numeroMovil
    
    def setNumeroMovil(self, nuevoTelefonoMovil):
        self.__numeroMovil = nuevoTelefonoMovil
        return self.__numeroMovil
    
    def __str__(self):
        return "Su nombre es: " + str(self.getNombre()) + " " + str(self.getApellidos()) + "\n" + "Con numero de movil: " + str(self.getNumeroMovil()) + str(self.direccionContacto.__str__())
